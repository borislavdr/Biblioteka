var bibliotekaApp = angular.module('bibliotekaApp', ['ngRoute']);
	

bibliotekaApp.config(['$routeProvider', function($routeProvider) {
	
	$routeProvider
	
		.when('/', {
			templateUrl : '/static/app/html/partial/home.html'
		})
		
		.when('/biblioteke', {
			templateUrl : '/static/app/html/partial/biblioteka.html',
			controller: 'bibliotekaController'
		})
		
		.when('/knjige', {
			templateUrl : '/static/app/html/partial/view-knjiga.html',
			controller: 'knjigaController'
		})
		
		.when('/clanovi', {
			templateUrl : '/static/app/html/partial/view-clanovi.html',
			controller: 'clanoviController'
		})
		
		.when('/biblioteke/view/:id', {
			templateUrl : '/static/app/html/partial/view-biblioteka.html',
			controller: 'viewBibliotekaController'
		})
		
		.when('/biblioteka/knjige/view/:id', {
			templateUrl : '/static/app/html/partial/view-knjiga.html',
			controller: 'knjigaController'
		})
		
		.when('/biblioteka/clanovi/view/:bibliotekaId', {
			templateUrl : '/static/app/html/partial/view-clanovi.html',
			controller: 'clanoviController'
		})
		
		.when('/biblioteka/add', {
			templateUrl : '/static/app/html/partial/add-biblioteka.html',
			controller: 'addBibliotekaController'
		})
		
		.otherwise({
			redirectTo: '/'
		});
}]);