angular.module('bibliotekaApp')
.controller('bibliotekaController',['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
	
	$scope.biblioteke = [];
	
	$scope.base_url = '/api/biblioteka';
	//$scope.base_url_knjige = 'api/knjiga';
	
	var getBiblioteke = function() {
		$http.get($scope.base_url)
		
			.then(function success(data) {
				$scope.biblioteke = data.data;
			}, function error(data) {
				console.log(data);
			})
	};
	
	getBiblioteke();
	
	$scope.getBiblioteka = function(id) {
		$http.get($scope.base_url + "/" + id)
		.then(function success(data) {
			$location.path("/biblioteke/view/" + id);
		}, function error(data) {
			console.log(data);
		})
	};
	
	$scope.deleteBiblioteka = function(id) {
		$http.delete($scope.base_url + '/' + id)
			.then(function success(data) {
				getBiblioteke();
			}, function error(data) {
				console.log(data);
			})
	};
	
//	$scope.viewBiblioteka = function(id){
//		$location.path("/biblioteka/view/" + id);
//	};
	
	
	$scope.navigateToKnjige = function(bibliotekaId) {	
		$location.path("/biblioteka/knjige/view/" + bibliotekaId);
	};
	
	$scope.navigateToClanovi = function(bibliotekaId) {
		$location.path("/biblioteka/clanovi/view/" + bibliotekaId);
	};
	
	$scope.navigateToAddBiblioteka = function() {
		$location.path("/biblioteka/add");
	};
	
}]);


bibliotekaApp.controller('viewBibliotekaController',['$scope', '$http','$routeParams', function($scope, $http, $routeParams) {
	
//	$scope.biblioteke = [];
	
	$scope.biblioteka = {};
	
	$scope.base_url = 'api/biblioteka';
	
//	var getBiblioteke = function() {
//		$http.get($scope.base_url)
//		.then(function success(data) {
//			$scope.biblioteke = data.data;
//		}, function error(data) {
//			console.log(data);
//		})
//	};
//	
//	getBiblioteke();
	
	var getBiblioteka = function(id) {
		$http.get($scope.base_url + "/" + $routeParams.id)
		.then(function success(data) {
			$scope.biblioteka = data.data;
		}, function error(data) {
			console.log(data)
		})
	};
	
	getBiblioteka();
	
}]);

bibliotekaApp.controller('addBibliotekaController', ['$scope', '$http', '$location', function($scope, $http, $location) {
	
	$scope.base_url = "/api/biblioteka";
	
	$scope.biblioteka = {};
	
	$scope.add = function() {
		$http.post($scope.base_url, $scope.biblioteka)
		.then(function success(data) {
			$location.path()
		})
	}
	
}]);
