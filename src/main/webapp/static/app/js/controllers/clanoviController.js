bibliotekaApp.controller('clanoviController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
	
	$scope.base_url = '/api/biblioteka';
	$scope.base_url_clanovi = '/api/clan';
	
	var getClanovi = function() {
		$http.get($scope.base_url_clanovi)
		.then(function success(data) {
			$scope.clanovi = data.data;
		}, function error(data) {
			console.log(data);
		})
	};
	
	getClanovi();
	
	function getClanoviByBibliotekaId(bibliotekaId) {
		$http.get($scope.base_url + '/' + $routeParams.bibliotekaId + '/clanovi')
		.then (function success(data) {
			console.log = data;
			$scope.clanovi = data.data;
		}, function error(data) {
			console.log(data);
		})
	};
	
	getClanoviByBibliotekaId();
	
}]);