package jwd.biblioteka.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jwd.biblioteka.model.Clan;
import jwd.biblioteka.repository.ClanRepository;
import jwd.biblioteka.service.ClanService;

@Service
@Transactional
public class ClanServiceImpl implements ClanService {

	@Autowired
	ClanRepository clanRepository;

	@Override
	public List<Clan> findAll() {
		return clanRepository.findAll();
	}

	@Override
	public Clan findOne(Long id) {
		return clanRepository.findOne(id);
	}

	@Override
	public void save(Clan clan) {
		clanRepository.save(clan);

	}

	@Override
	public void delete(Long id) {
		clanRepository.delete(id);

	}

	@Override
	public Page<Clan> findClanoviForBiblioteka(Long bibliotekaId, Pageable page) {
		return clanRepository.findClanoviForBiblioteka(bibliotekaId, page);
	}

}
