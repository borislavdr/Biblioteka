package jwd.biblioteka.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.repository.KnjigaRepository;
import jwd.biblioteka.service.KnjigaService;

@Service
@Transactional
public class KnjigaServiceImpl implements KnjigaService {

	@Autowired
	KnjigaRepository knjigaRepository;

	@Override
	public List<Knjiga> findAll() {
		return knjigaRepository.findAll();
	}

	@Override
	public Knjiga findOne(Long id) {
		return knjigaRepository.findOne(id);
	}

	@Override
	public void save(Knjiga knjiga) {
		knjigaRepository.save(knjiga);

	}

	@Override
	public void delete(Long id) {
		knjigaRepository.delete(id);

	}

	@Override
	public Page<Knjiga> findKnjigeByBibliotekaId(Long id, Pageable page) {
		return knjigaRepository.findKnjigeByBibliotekaId(id, page);
	}

}
