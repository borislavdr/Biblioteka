package jwd.biblioteka.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.repository.BibliotekaRepository;
import jwd.biblioteka.service.BibliotekaService;

@Service
@Transactional
public class BibliotekaServiceImpl implements BibliotekaService {

	@Autowired
	BibliotekaRepository bibliotekaRepository;

	@Override
	public List<Biblioteka> findAll() {
		return bibliotekaRepository.findAll();
	}

	@Override
	public Biblioteka findOne(Long id) {
		return bibliotekaRepository.findOne(id);
	}

	@Override
	public void save(Biblioteka biblioteka) {
		bibliotekaRepository.save(biblioteka);

	}

	@Override
	public void delete(Long id) {
		bibliotekaRepository.delete(id);

	}

//	@Override
//	public Page<Knjiga> getKnjigeFromB(Long id, Pageable page) {
//		return bibliotekaRepository.getKnjigeFromB(id, page);
//	}

//	@Override
//	public List<Knjiga> findKnjigeByBibliotekaID(Long id) {
//		return bibliotekaRepository.findKnjigeById(id);
//	}


}
