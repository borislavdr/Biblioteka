package jwd.biblioteka.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jwd.biblioteka.model.Knjiga;

public interface KnjigaService {

	List<Knjiga> findAll();
	Knjiga findOne(Long id);
	void save(Knjiga knjiga);
	void delete(Long id);

	public Page<Knjiga> findKnjigeByBibliotekaId(Long id, Pageable page);

}
