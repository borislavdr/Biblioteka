package jwd.biblioteka.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jwd.biblioteka.model.Clan;

public interface ClanService {

	List<Clan> findAll();
	Clan findOne(Long id);
	void save(Clan clan);
	void delete(Long id);
	
	public Page<Clan> findClanoviForBiblioteka(Long bibliotekaId, Pageable page);
}
