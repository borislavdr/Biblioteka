package jwd.biblioteka.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.model.Knjiga;

public interface BibliotekaService {

	 List<Biblioteka> findAll();
	 Biblioteka findOne(Long id);
	 void save(Biblioteka biblioteka);
	 void delete(Long id);
	 
	 //public Page<Knjiga> getKnjigeFromB(Long id, Pageable page);



}
