//package jwd.biblioteka;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import jwd.biblioteka.model.Biblioteka;
//import jwd.biblioteka.model.Clan;
//import jwd.biblioteka.model.Knjiga;
//import jwd.biblioteka.service.BibliotekaService;
//import jwd.biblioteka.service.ClanService;
//
//@Component
//public class TestData {
//
//	@Autowired
//	private BibliotekaService bibliotekaService;
//
//	@Autowired
//	private ClanService clanService;
//
//	@PostConstruct
//	public void init(){
//
//		Clan c1 = new Clan();
//		c1.setIme("Pera");
//		c1.setPrezime("Peric");
//		c1.setClKarta("123A");
//		c1.setAdresa("Cara Dusana 7, Indjija");
//		c1.setTelefon("069/11-11-1111");
//		c1.setEmail("pera@gmail.com");
//		clanService.save(c1);
//
//		Clan c2 = new Clan();
//		c2.setIme("Mika");
//		c2.setPrezime("Mikic");
//		c2.setClKarta("156B");
//		c2.setAdresa("Bulevar Oslobodjenja 23, Novi Sad");
//		c2.setTelefon("064/00-00-000");
//		c2.setEmail("mika@gmail.com");
//		clanService.save(c2);
//
//		Biblioteka b1 = new Biblioteka();
//
//		b1.setNaziv("Djordje Natosevic");
//		b1.setMesto("Indjija");
//		b1.setRadnoVreme("09-17h");
//		b1.setAdresa("Karadjordjeva 15");
//		b1.setBrojTelefona("022/587-698");
//		b1.setEmail("dnato.@mail.com");
//		b1.addClan(c1);
//
//		Knjiga k1 = new Knjiga();
//
//		k1.setNaziv("Kroz pustinju i prasumu");
//		k1.setAutor("Henrih Sjenkjevic");
//		k1.setBrojPrimeraka(5);
//		k1.setIzdatih(3);
//		k1.setBiblioteka(b1);
//
//		Knjiga k2 = new Knjiga();
//
//		k2.setNaziv("Na Drini cuprija");
//		k2.setAutor("Ivo Andric");
//		k2.setBrojPrimeraka(7);
//		k2.setIzdatih(2);
//		k2.setBiblioteka(b1);
//
//		bibliotekaService.save(b1);
//
//		Biblioteka b2 = new Biblioteka();
//
//		b2.setNaziv("Laza Kostic");
//		b2.setMesto("Novi Sad");
//		b2.setRadnoVreme("09-17h");
//		b2.setAdresa("Dunavska 1");
//		b2.setBrojTelefona("021/698-9658");
//		b2.setEmail("lkost@mail.com");
//		b2.addClan(c1);
//		b2.addClan(c2);
//
//		Knjiga k3 = new Knjiga();
//		k3.setNaziv("Seobe");
//		k3.setAutor("Milos Crnjanski");
//		k3.setBrojPrimeraka(4);
//		k3.setIzdatih(1);
//		k3.setBiblioteka(b2);
//
//		Knjiga k4 = new Knjiga();
//		k4.setNaziv("Rat i Mir");
//		k4.setAutor("Lav Tolstoj");
//		k4.setBrojPrimeraka(10);
//		k4.setIzdatih(3);
//		k4.setBiblioteka(b2);
//
//		Knjiga k5 = new Knjiga();
//		k5.setNaziv("Kockar");
//		k5.setAutor("Fjodor Mihailovic Dostojevski");
//		k5.setBrojPrimeraka(7);
//		k5.setIzdatih(1);
//		k5.setBiblioteka(b2);
//
//		bibliotekaService.save(b2);
//
//	}
//}
