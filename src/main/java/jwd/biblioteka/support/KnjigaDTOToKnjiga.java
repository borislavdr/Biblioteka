package jwd.biblioteka.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.service.BibliotekaService;
import jwd.biblioteka.service.KnjigaService;
import jwd.biblioteka.web.dto.KnjigaDTO;

@Component
public class KnjigaDTOToKnjiga implements Converter<KnjigaDTO, Knjiga> {

	@Autowired
	private BibliotekaService bibliotekaService;

	@Autowired
	private KnjigaService knjigaService;

	@Override
	public Knjiga convert(KnjigaDTO knjigaDTO) {

		Knjiga knjiga;

		if(knjigaDTO.getId() == null){
			knjiga = new Knjiga();
		}else{
			knjiga = knjigaService.findOne(knjigaDTO.getId());
		}

		knjiga.setId(knjigaDTO.getId());
		knjiga.setNaziv(knjigaDTO.getNaziv());
		knjiga.setAutor(knjigaDTO.getAutor());
		knjiga.setGodinaStampanja(knjigaDTO.getGodinaStampanja());
		knjiga.setGodinaZavodjenja(knjigaDTO.getGodinaZavodjenja());
		//knjiga.setDatumIzdavanja(knjigaDTO.getDatumIzdavanja());
		knjiga.setInventBroj(knjigaDTO.getInventBroj());
		knjiga.getBiblioteka().setId(knjigaDTO.getBibliotekaID());
		//knjiga.getClan().setId(knjigaDTO.getClanID());

		return knjiga;

	}

}
