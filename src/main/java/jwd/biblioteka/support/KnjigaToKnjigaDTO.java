	package jwd.biblioteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.web.dto.KnjigaDTO;

@Component
public class KnjigaToKnjigaDTO implements Converter<Knjiga, KnjigaDTO> {

	@Override
	public KnjigaDTO convert(Knjiga knjiga) {

		KnjigaDTO dto = new KnjigaDTO();
		dto.setId(knjiga.getId());
		dto.setNaziv(knjiga.getNaziv());
		dto.setAutor(knjiga.getAutor());
		dto.setGodinaStampanja(knjiga.getGodinaStampanja());
		dto.setGodinaZavodjenja(knjiga.getGodinaZavodjenja());
		//dto.setDatumIzdavanja(knjiga.getDatumIzdavanja());
		dto.setInventBroj(knjiga.getInventBroj());
		
		dto.setBibliotekaID(knjiga.getBiblioteka().getId());
		dto.setBibliotekaNaziv(knjiga.getBiblioteka().getNaziv());
		dto.setBibliotekaMesto(knjiga.getBiblioteka().getMesto());
		
//		dto.setClanID(knjiga.getClan().getId());
//		dto.setImeClana(knjiga.getClan().getIme());
//		dto.setPrezimeClana(knjiga.getClan().getPrezime());
//		dto.setJmbg(knjiga.getClan().getJmbg());

		return dto;
	}

	public List<KnjigaDTO> convert(List<Knjiga> knjige){

		List<KnjigaDTO> dtos = new ArrayList<>();

		for (Knjiga k : knjige) {
			dtos.add(convert(k));
		}

		return dtos;
	}

}
