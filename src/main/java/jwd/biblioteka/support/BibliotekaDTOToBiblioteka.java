package jwd.biblioteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.service.BibliotekaService;
import jwd.biblioteka.web.dto.BibliotekaDTO;

@Component
public class BibliotekaDTOToBiblioteka implements Converter<BibliotekaDTO, Biblioteka> {

	@Autowired
	private BibliotekaService bibliotekaService;

	@Override
	public Biblioteka convert(BibliotekaDTO bibliotekaDTO) {

		Biblioteka biblioteka = new Biblioteka();

		if(bibliotekaDTO.getId()!= null){
			biblioteka = bibliotekaService.findOne(bibliotekaDTO.getId());

			if(biblioteka == null){
				throw new IllegalStateException("Access to modification denied");
			}
		}

		biblioteka.setNaziv(bibliotekaDTO.getNaziv());
		biblioteka.setMesto(bibliotekaDTO.getMesto());
		biblioteka.setAdresa(bibliotekaDTO.getAdresa());
		biblioteka.setRadnoVreme(bibliotekaDTO.getRadnoVreme());
		biblioteka.setBrojTelefona(bibliotekaDTO.getBrojTelefona());
		biblioteka.setEmail(bibliotekaDTO.getEmail());

		return biblioteka;
	}

	public List<Biblioteka> convert (List<BibliotekaDTO> listDTO){

		List<Biblioteka> lista = new ArrayList<>();

		for (BibliotekaDTO b : listDTO) {
			lista.add(convert(b));
		}

		return lista;
	}

}
