package jwd.biblioteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Clan;
import jwd.biblioteka.web.dto.ClanDTO;

@Component
public class ClanToClanDTO implements Converter<Clan, ClanDTO> {

	@Override
	public ClanDTO convert(Clan clan) {

		ClanDTO dto = new ClanDTO();
		dto.setId(clan.getId());
		dto.setIme(clan.getIme());
		dto.setPrezime(clan.getPrezime());
		dto.setClKarta(clan.getClKarta());
		dto.setAdresa(clan.getAdresa());
		dto.setTelefon(clan.getTelefon());
		dto.setEmail(clan.getEmail());
		dto.setJmbg(clan.getJmbg());

		return dto;
	}

	public List<ClanDTO> convert(List<Clan> clanovi){

		List<ClanDTO> dtos = new ArrayList<>();

		for (Clan cl : clanovi) {
			dtos.add(convert(cl));
		}

		return dtos;
	}

}
