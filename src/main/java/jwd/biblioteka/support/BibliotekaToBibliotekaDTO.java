package jwd.biblioteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.web.dto.BibliotekaDTO;

@Component
public class BibliotekaToBibliotekaDTO implements Converter<Biblioteka, BibliotekaDTO> {


	@Override
	public BibliotekaDTO convert(Biblioteka biblioteka) {

		BibliotekaDTO dto = new BibliotekaDTO();

		dto.setId(biblioteka.getId());
		dto.setNaziv(biblioteka.getNaziv());
		dto.setMesto(biblioteka.getMesto());
		dto.setRadnoVreme(biblioteka.getRadnoVreme());
		dto.setAdresa(biblioteka.getAdresa());
		dto.setBrojTelefona(biblioteka.getBrojTelefona());
		dto.setEmail(biblioteka.getEmail());

		return dto;
	}

	public List<BibliotekaDTO> convert(List<Biblioteka> biblioteke){

		List<BibliotekaDTO> dtos = new ArrayList<>();

		for (Biblioteka b : biblioteke) {
			dtos.add(convert(b));
		}

		return dtos;
	}

}
