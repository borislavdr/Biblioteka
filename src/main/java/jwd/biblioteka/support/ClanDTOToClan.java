package jwd.biblioteka.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.model.Clan;
import jwd.biblioteka.service.ClanService;
import jwd.biblioteka.web.dto.BibliotekaDTO;
import jwd.biblioteka.web.dto.ClanDTO;

@Component
public class ClanDTOToClan implements Converter<ClanDTO, Clan> {
	
	@Autowired
	private ClanService clanService;

	@Override
	public Clan convert(ClanDTO dto) {

		Clan clan = new Clan();
		
		if(dto.getId()!= null){
			clan = clanService.findOne(dto.getId());
			
			if(clan == null){
				throw new IllegalStateException("Access to modification denied.");
			}
		}
		
		clan.setIme(dto.getIme());
		clan.setPrezime(dto.getPrezime());
		clan.setAdresa(dto.getAdresa());
		clan.setClKarta(dto.getClKarta());
		clan.setEmail(dto.getEmail());
		clan.setTelefon(dto.getTelefon());
		
		return clan;
	}

	public List<Clan> convert (List<ClanDTO> listDTO){

		List<Clan> lista = new ArrayList<>();

		for (ClanDTO c : listDTO) {
			lista.add(convert(c));
		}

		return lista;
	}
	
}
