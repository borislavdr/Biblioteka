package jwd.biblioteka.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "biblioteka")
public class Biblioteka {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column()
	private Long id;
	@Column
	private String naziv;
	@Column
	private String mesto;
	@Column
	private String adresa;
	@Column
	private String radnoVreme;
	@Column
	private String brojTelefona;
	@Column
	private String email;

	@OneToMany(mappedBy="biblioteka", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Knjiga> knjige;

	//@OneToMany(mappedBy="biblioteka", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@ManyToMany(mappedBy = "biblioteke", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Clan> clanovi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public String getRadnoVreme() {
		return radnoVreme;
	}

	public void setRadnoVreme(String radnoVreme) {
		this.radnoVreme = radnoVreme;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public List<Knjiga> getKnjige() {
		return knjige;
	}

	public void setKnjige(List<Knjiga> knjige) {
		this.knjige = knjige;
	}

	public List<Clan> getClanovi() {
		return clanovi;
	}

	public void setClanovi(List<Clan> clanovi) {
		this.clanovi = clanovi;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public void addKnjiga(Knjiga knjiga) {
//		this.knjige.add(knjiga);
//
//		if (!knjiga.equals(knjiga.getBiblioteka())) {
//			knjiga.setBiblioteka(this);
//		}
//	}
//
//	public void removeKnjiga(Knjiga knjiga) {
//
//		if (knjige.contains(knjiga)) {
//			knjiga.setBiblioteka(null);
//			knjige.remove(knjiga);
//		}
//	}
//
//	public void addClan(Clan clan){
//		this.clanovi.add(clan);
//	}

//	public void removeBibliotekeFromClanovi() {
//
//		for (Clan c : clanovi) {
//			c.getBiblioteke().remove(this);
//		}
//	}
}
