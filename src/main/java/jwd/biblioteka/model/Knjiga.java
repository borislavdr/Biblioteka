package jwd.biblioteka.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="knjiga")
public class Knjiga {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private String autor;
	@Column
	private Integer godinaStampanja;
	@Column
	private Integer godinaZavodjenja;
//	@Column
//	private String datumIzdavanja;
	@Column
	private String inventBroj;

	@ManyToOne
	private Biblioteka biblioteka;
	
//	@ManyToOne
//	private Clan clan;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public Integer getGodinaStampanja() {
		return godinaStampanja;
	}
	public void setGodinaStampanja(Integer godinaStampanja) {
		this.godinaStampanja = godinaStampanja;
	}
//	public String getDatumIzdavanja() {
//		return datumIzdavanja;
//	}
//	public void setDatumIzdavanja(String datumIzdavanja) {
//		this.datumIzdavanja = datumIzdavanja;
//	}
	public Integer getGodinaZavodjenja() {
		return godinaZavodjenja;
	}
	public void setGodinaZavodjenja(Integer godinaZavodjenja) {
		this.godinaZavodjenja = godinaZavodjenja;
	}
	public Biblioteka getBiblioteka() {
		return biblioteka;
	}
	public void setBiblioteka(Biblioteka biblioteka) {
		this.biblioteka = biblioteka;
	}
//	public Clan getClan() {
//		return clan;
//	}
//	public void setClan(Clan clan) {
//		this.clan = clan;
//	}
	public String getInventBroj() {
		return inventBroj;
	}
	public void setInventBroj(String inventBroj) {
		this.inventBroj = inventBroj;
	}
	

//		if(biblioteka != null && !biblioteka.getKnjige().contains(this)){
//			biblioteka.getKnjige().add(this);
//		}
	}



