package jwd.biblioteka.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="clanovi")
public class Clan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column()
	private Long id;
	@Column
	private String ime;
	@Column
	private String prezime;
	@Column
	private String clKarta;
	@Column
	private String adresa;
	@Column
	private String telefon;
	@Column
	private String email;
	@Column
	private Long jmbg;

	@ManyToMany
	private List<Biblioteka> biblioteke;

//	@OneToMany(mappedBy = "clan", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	private List<Knjiga> knjige;
//	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getClKarta() {
		return clKarta;
	}
	public void setClKarta(String clKarta) {
		this.clKarta = clKarta;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getJmbg() {
		return jmbg;
	}
	public void setJmbg(Long jmbg) {
		this.jmbg = jmbg;
	}
	public List<Biblioteka> getBiblioteke() {
		return biblioteke;
	}
	public void setBiblioteke(List<Biblioteka> biblioteke) {
		this.biblioteke = biblioteke;
	}
//	public List<Knjiga> getKnjige() {
//		return knjige;
//	}
//	public void setKnjige(List<Knjiga> knjige) {
//		this.knjige = knjige;
//	}
	
//
//	public void addBiblioteka(Biblioteka biblioteka){
//		this.biblioteke.add(biblioteka);

//		if(!biblioteka.equals(biblioteka.getClanovi())){
//			biblioteka.setClanovi(clanovi);
//		}
//	}

//	public void removeBibliotekeFromClanovi(Biblioteka biblioteka) {
//
//		for (Biblioteka b : biblioteke) {
//			b.getClanovi().remove(biblioteka);
//		}
//	    for (User u : users) {
//	        u.getGroups().remove(this);
//	    }
//	}
}
