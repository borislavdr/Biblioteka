package jwd.biblioteka.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.service.KnjigaService;
import jwd.biblioteka.support.KnjigaDTOToKnjiga;
import jwd.biblioteka.support.KnjigaToKnjigaDTO;
import jwd.biblioteka.web.dto.KnjigaDTO;

@RestController
@RequestMapping("api/knjiga")
public class ApiKnjigaController {

	@Autowired
	private KnjigaToKnjigaDTO toDTO;

	@Autowired
	private KnjigaDTOToKnjiga toKnjiga;

	@Autowired
	private KnjigaService knjigaService;


	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<KnjigaDTO>> findAll(){

		List<Knjiga> knjige = new ArrayList<>();

		knjige = knjigaService.findAll();

		return new ResponseEntity<>(toDTO.convert(knjige), HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<KnjigaDTO> findOne(@PathVariable Long id){

		Knjiga knjiga = knjigaService.findOne(id);

		if(knjiga == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toDTO.convert(knjiga), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, consumes="application/json")
	public ResponseEntity<KnjigaDTO> addOne(@RequestBody KnjigaDTO dto){

		Knjiga knjiga = new Knjiga();

		knjiga = toKnjiga.convert(dto);

		knjigaService.save(knjiga);

		return new ResponseEntity<>(toDTO.convert(knjiga), HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "{/id}", consumes = "application/json")
	public ResponseEntity<KnjigaDTO> updateOne(@PathVariable Long id, 
			@RequestBody KnjigaDTO dto){
		
		Knjiga knjiga = knjigaService.findOne(id);
		
		if(knjiga.equals(null)){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
		knjiga.setNaziv(dto.getNaziv());
		knjiga.setAutor(dto.getAutor());
		knjiga.setGodinaStampanja(dto.getGodinaStampanja());
		knjiga.setGodinaZavodjenja(dto.getGodinaZavodjenja());
		//knjiga.setDatumIzdavanja(dto.getDatumIzdavanja());
		knjiga.setInventBroj(dto.getInventBroj());
		knjiga.getBiblioteka().setId(dto.getBibliotekaID());
//		knjiga.getBiblioteka().setNaziv(dto.getBibliotekaNaziv());
//		knjiga.getBiblioteka().setMesto(dto.getBibliotekaMesto());
		//knjiga.getClan().setId(dto.getClanID());
		
		knjigaService.save(knjiga);
		
		return new ResponseEntity<>(toDTO.convert(knjiga), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Knjiga> removeOne(@PathVariable Long id){
		
		Knjiga knjiga = knjigaService.findOne(id);
		
		if(knjiga.equals(null)){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		knjigaService.delete(id);
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
//	@RequestMapping(method=RequestMethod.GET, value="/biblioteka/{id}")
//	public ResponseEntity<List<KnjigaDTO>> findKnjigeByBibliotekaId(@PathVariable Long id, Pageable page) {
//		
//		Page<Knjiga> knjigePage = knjigaService.findKnjigeByBibliotekaId(id, page);
//		
//		List<KnjigaDTO> knjigeDTO = toDTO.convert(knjigePage.getContent());
//		
//		return new ResponseEntity<>(knjigeDTO, HttpStatus.OK);
//	}
	
}
