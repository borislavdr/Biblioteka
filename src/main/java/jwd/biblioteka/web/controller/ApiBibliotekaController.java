package jwd.biblioteka.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.model.Clan;
import jwd.biblioteka.model.Knjiga;
import jwd.biblioteka.service.BibliotekaService;
import jwd.biblioteka.service.ClanService;
import jwd.biblioteka.service.KnjigaService;
import jwd.biblioteka.support.BibliotekaDTOToBiblioteka;
import jwd.biblioteka.support.BibliotekaToBibliotekaDTO;
import jwd.biblioteka.support.ClanToClanDTO;
import jwd.biblioteka.support.KnjigaToKnjigaDTO;
import jwd.biblioteka.web.dto.BibliotekaDTO;
import jwd.biblioteka.web.dto.ClanDTO;
import jwd.biblioteka.web.dto.KnjigaDTO;

@RestController
@RequestMapping("/api/biblioteka")
public class ApiBibliotekaController {

	@Autowired
	private BibliotekaService bibliotekaService;

	@Autowired
	private BibliotekaToBibliotekaDTO toDto;

	@Autowired
	private BibliotekaDTOToBiblioteka toBibl;
	
	@Autowired
	private KnjigaToKnjigaDTO toKnjigaDto;

	@Autowired
	private ClanService clanService;
	
	@Autowired
	private KnjigaService knjigaService;

	@Autowired
	private ClanToClanDTO toClanDTO;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<BibliotekaDTO>> getAll(){

		List<Biblioteka> bibl = bibliotekaService.findAll();

		return new ResponseEntity<>(toDto.convert(bibl), HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<BibliotekaDTO> findOne(@PathVariable Long id){

		Biblioteka bibl = bibliotekaService.findOne(id);

		if(bibl == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDto.convert(bibl), HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<BibliotekaDTO> addOne(@RequestBody BibliotekaDTO dto){

		//Biblioteka biblioteka = new Biblioteka();

		Biblioteka biblioteka = toBibl.convert(dto);

		bibliotekaService.save(biblioteka);

		return new ResponseEntity<>(toDto.convert(biblioteka), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, value="/{id}", consumes="application/json")
	public ResponseEntity<BibliotekaDTO> updateOne(
							@PathVariable Long id,
							@RequestBody BibliotekaDTO dto){


		Biblioteka biblioteka = bibliotekaService.findOne(id);

		if(biblioteka==null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		//biblioteka = toBibl.convert(dto);

		biblioteka.setNaziv(dto.getNaziv());
		biblioteka.setMesto(dto.getMesto());
		biblioteka.setAdresa(dto.getAdresa());
		biblioteka.setRadnoVreme(dto.getRadnoVreme());
		biblioteka.setBrojTelefona(dto.getBrojTelefona());
		biblioteka.setEmail(dto.getEmail());

		bibliotekaService.save(biblioteka);

		return new ResponseEntity<>(toDto.convert(biblioteka), HttpStatus.CREATED);
	}

	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Biblioteka> removeOne(@PathVariable Long id){

		Biblioteka biblioteka = bibliotekaService.findOne(id);

		if(biblioteka == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		//biblioteka.removeBibliotekeFromClanovi();

		bibliotekaService.delete(id);

		//clanService.save(clan);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/knjige")
	public ResponseEntity<List<KnjigaDTO>> getKnjigeByBibliotekaId(@PathVariable Long id, Pageable page) {
		
	    Page<Knjiga> knjigePage = knjigaService.findKnjigeByBibliotekaId(id, page);
	    
	    List<Knjiga> knjigeList = knjigePage.getContent();
		
		return new ResponseEntity<>(toKnjigaDto.convert(knjigeList), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{bibliotekaId}/clanovi")
	public ResponseEntity<List<ClanDTO>> getClanoviByBiblioteka(@PathVariable Long bibliotekaId, Pageable page) {
		
		Page<Clan> clanoviPage = clanService.findClanoviForBiblioteka(bibliotekaId, page);
		
		List<Clan> clanoviLista = clanoviPage.getContent();
		
		return new ResponseEntity<>(toClanDTO.convert(clanoviLista), HttpStatus.OK);
	}
}
