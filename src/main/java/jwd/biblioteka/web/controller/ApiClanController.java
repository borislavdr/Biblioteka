package jwd.biblioteka.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jwd.biblioteka.model.Clan;
import jwd.biblioteka.repository.ClanRepository;
import jwd.biblioteka.service.ClanService;
import jwd.biblioteka.support.ClanDTOToClan;
import jwd.biblioteka.support.ClanToClanDTO;
import jwd.biblioteka.web.dto.ClanDTO;

@RestController
@RequestMapping("/api/clan")
public class ApiClanController {

	@Autowired
	private ClanService clanService;
	
	@Autowired
	private ClanToClanDTO toDto;
	
	@Autowired
	private ClanDTOToClan toClan;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ClanDTO>> findAll(){
		
		List<Clan> clan = clanService.findAll();
		
		return new ResponseEntity<>(toDto.convert(clan), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/{id}")
	public ResponseEntity<ClanDTO> findOne(@PathVariable Long id){
		
		Clan clan = clanService.findOne(id);
		
		if(clan.equals(null)){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDto.convert(clan), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ClanDTO> save(@RequestBody ClanDTO dto){
		
		Clan clan = new Clan();
		
		clan = toClan.convert(dto);
		
		clanService.save(clan);
		
		return new ResponseEntity<>(toDto.convert(clan), HttpStatus.CREATED);
		
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = "application/json")
	public ResponseEntity<ClanDTO> updateOne(
			              @PathVariable Long id, 
			              @RequestBody ClanDTO dto){
		
		Clan clan = clanService.findOne(id);
		
		if(clan == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		clan.setIme(dto.getIme());
		clan.setPrezime(dto.getPrezime());
		clan.setAdresa(dto.getAdresa());
		clan.setClKarta(dto.getClKarta());
		clan.setEmail(dto.getEmail());
		clan.setTelefon(dto.getTelefon());
		
		clanService.save(clan);
		
		return new ResponseEntity<>(toDto.convert(clan), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}")
	public ResponseEntity<Clan> delete(@PathVariable Long id){
		
		Clan clan = clanService.findOne(id);
		
		if(clan.equals(null)){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		clanService.delete(id);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
//	@RequestMapping(method=RequestMethod.GET, value="/biblioteka/{bibliotekaId}")
//	public ResponseEntity<List<ClanDTO>> findClanoviForBiblioteka(@PathVariable Long bibliotekaId, Pageable page) {
//		
//		Page<Clan> clanovi = clanService.findClanoviForBiblioteka(bibliotekaId, page);
//		
//		List<ClanDTO> clanoviDTO = toDto.convert(clanovi.getContent());
//		
//		return new ResponseEntity<>(clanoviDTO, HttpStatus.OK);
//	}
}
