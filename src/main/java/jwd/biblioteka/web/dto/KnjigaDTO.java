package jwd.biblioteka.web.dto;

public class KnjigaDTO {

	private Long id;
	private String naziv;
	private String autor;
	private Integer godinaStampanja;
	private Integer godinaZavodjenja;
	//private String datumIzdavanja;
	private String inventBroj;
	
	private Long bibliotekaID;
	private String bibliotekaNaziv;
	private String bibliotekaMesto;
	
//	private Long clanID;
//	private String imeClana;
//	private String prezimeClana;
//	private Long jmbg;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public Long getBibliotekaID() {
		return bibliotekaID;
	}
	public void setBibliotekaID(Long bibliotekaID) {
		this.bibliotekaID = bibliotekaID;
	}
	public String getBibliotekaNaziv() {
		return bibliotekaNaziv;
	}
	public void setBibliotekaNaziv(String bibliotekaNaziv) {
		this.bibliotekaNaziv = bibliotekaNaziv;
	}
	public String getBibliotekaMesto() {
		return bibliotekaMesto;
	}
	public void setBibliotekaMesto(String bibliotekaMesto) {
		this.bibliotekaMesto = bibliotekaMesto;
	}
	public Integer getGodinaStampanja() {
		return godinaStampanja;
	}
	public void setGodinaStampanja(Integer godinaStampanja) {
		this.godinaStampanja = godinaStampanja;
	}
//	public String getDatumIzdavanja() {
//		return datumIzdavanja;
//	}
//	public void setDatumIzdavanja(String datumIzdavanja) {
//		this.datumIzdavanja = datumIzdavanja;
//	}
	public Integer getGodinaZavodjenja() {
		return godinaZavodjenja;
	}
	public void setGodinaZavodjenja(Integer godinaZavodjenja) {
		this.godinaZavodjenja = godinaZavodjenja;
	}
	public String getInventBroj() {
		return inventBroj;
	}
	public void setInventBroj(String inventBroj) {
		this.inventBroj = inventBroj;
	}
//	public Long getClanID() {
//		return clanID;
//	}
//	public void setClanID(Long clanID) {
//		this.clanID = clanID;
//	}
//	public String getImeClana() {
//		return imeClana;
//	}
//	public void setImeClana(String imeClana) {
//		this.imeClana = imeClana;
//	}
//	public String getPrezimeClana() {
//		return prezimeClana;
//	}
//	public void setPrezimeClana(String prezimeClana) {
//		this.prezimeClana = prezimeClana;
//	}
//	public Long getJmbg() {
//		return jmbg;
//	}
//	public void setJmbg(Long jmbg) {
//		this.jmbg = jmbg;
//	}

}
