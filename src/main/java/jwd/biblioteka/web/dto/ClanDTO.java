package jwd.biblioteka.web.dto;

public class ClanDTO {

	private Long id;
	private String ime;
	private String prezime;
	private String clKarta;
	private String adresa;
	private String telefon;
	private String email;
	private Long jmbg;
//	private Long bibliotekaID;
//	private String bibliotekaNaziv;
//	private String bibliotekaMesto;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getClKarta() {
		return clKarta;
	}
	public void setClKarta(String clKarta) {
		this.clKarta = clKarta;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getJmbg() {
		return jmbg;
	}
	public void setJmbg(Long jmbg) {
		this.jmbg = jmbg;
	}
//	public Long getBibliotekaID() {
//		return bibliotekaID;
//	}
//	public void setBibliotekaID(Long bibliotekaID) {
//		this.bibliotekaID = bibliotekaID;
//	}
//	public String getBibliotekaNaziv() {
//		return bibliotekaNaziv;
//	}
//	public void setBibliotekaNaziv(String bibliotekaNaziv) {
//		this.bibliotekaNaziv = bibliotekaNaziv;
//	}
//	public String getBibliotekaMesto() {
//		return bibliotekaMesto;
//	}
//	public void setBibliotekaMesto(String bibliotekaMesto) {
//		this.bibliotekaMesto = bibliotekaMesto;
//	}

}
