package jwd.biblioteka.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd.biblioteka.model.Biblioteka;
import jwd.biblioteka.model.Knjiga;

@Repository
public interface BibliotekaRepository extends
							JpaRepository<Biblioteka, Long> {

	//public Page<Knjiga> findKnjigeByBiblioteka(Long id, Pageable page);

	//public Page<Knjiga> getKnjigeFromB(Long id, Pageable page);
}
