package jwd.biblioteka.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.biblioteka.model.Clan;

@Repository
public interface ClanRepository extends JpaRepository<Clan, Long> {

	@Query("select c "
			+ "from Clan c "
			+ "left join c.biblioteke b "
			+ "where b.id = :bibliotekaId ")
	public Page<Clan> findClanoviForBiblioteka(@Param("bibliotekaId") Long bibliotekaId, Pageable page);
}
