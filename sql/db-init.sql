create database api_library;

use api_library;

insert into biblioteka (adresa, broj_telefona, email, mesto, naziv, radno_vreme) values ('Narodnog fronta 47', '021/5856-365', 'dkis@mail.com', 'Novi Sad', 'Danilo Kis', '08-20h');
insert into biblioteka (adresa, broj_telefona, email, mesto, naziv, radno_vreme) values ('Dunavska 1', '021/568-865', 'gradskabibl@mail.com', 'Novi Sad', 'Gradska Biblioteka', '08-20h');

insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Francuska 5', 'dkj55', 'joey@mail.com', 'Joey', 123, 'Tribbiani', '065/98564-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Srpska 7', 'dkr78', 'rachel@mail.com', 'Rachel', 456, 'Green', '062/33264-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Irska 10', 'dkr01', 'ross@mail.com', 'Ross', 321, 'Geller', '063/86964-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Norveska 3', 'dkm33', 'monika@mail.com', 'Monika', 789, 'Geller', '064/9836-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Americka 1', 'dkc95', 'chandler@mail.com', 'Chandler', 654, 'Bing', '066/93214-89');

insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Poljska 3', 'grd75', 'delboy@mail.com', 'Derek', 1011, 'Trotter', '066/9354-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Finska 11', 'grr63', 'rodney@mail.com', 'Rodney', 1112, 'Trotter', '064/94264-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Norveska 3', 'grm85', 'monika@mail.com', 'Monika', 789, 'Geller', '064/9836-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Americka 1', 'grc100', 'chandler@mail.com', 'Chandler', 654, 'Bing', '066/93214-89');
insert into clanovi (adresa, cl_karta, email, ime, jmbg, prezime, telefon) values ('Irska 10', 'grr013', 'ross@mail.com', 'Ross', 321, 'Geller', '063/86964-89');

-- use api_library;

insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1985, "dkrm/001", "Rat i Mir", 1);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Aleksandar Dima", 1980, 1990, "dkmk/001", "Grof Monte Kristo", 1);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Ivo Andric", 1970, 1985, "dkdc/001", "Na Drini cuprija", 1);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Zil Vern", 1980, 1985, "dkmp/001", "20 000 milja pod morem", 1);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1985, "dkrm/002", "Rat i Mir", 1);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1985, "dkrm/003", "Rat i Mir", 1);

insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Ivo Andric", 1970, 1990, "grdc/001", "Na Drini cuprija", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1980, "grrm/001", "Rat i Mir", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Zil Vern", 1980, 1995, "grmp/001", "20 000 milja pod morem", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Zil Vern", 1965, 1985, "grmp/002", "20 000 milja pod morem", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1985, "grrm/002", "Rat i Mir", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Lav Tolstoj", 1965, 1985, "grrm/003", "Rat i Mir", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Ivo Andric", 1970, 1995, "grrm/002", "Na Drini cuprija", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("Henrih Sjenkjevic", 1965, 1985, "grkpp/001", "Kroz pustinju i prasumu", 2);
insert into knjiga (autor, godina_stampanja, godina_zavodjenja, invent_broj, naziv, biblioteka_id) values ("F. M. Dostojevski", 1965, 1985, "grzk/001", "Zlocin i Kazna", 2);

insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (1, 1);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (2, 1);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (3, 1);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (4, 1);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (5, 1);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (6, 2);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (7, 2);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (8, 2);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (9, 2);
insert into clanovi_biblioteke (clanovi_id, biblioteke_id) values (10, 2);

insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("01/01/2017", 1, 1);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("02/02/2017", 1, 2);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("03/03/2017", 3, 5);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("04/04/2017", 7, 7);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("05/05/2017", 7, 8);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("01/01/2017", 7, 9);
insert into iznajmljeno (datum_izdavanja, clan_id, knjiga_id) values ("03/03/2017", 9, 10);


